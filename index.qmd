---
title: "scRNASeq"
author: Laurent Modolo
date: 2022-2023
---

```{r setup, include=F}
knitr::opts_chunk$set(
  echo = T,
  warning = F,
  message = F,
  cache = T,
  root.dir = "..",
  fit.width = 10,
  fig.height = 5,
  fig.path = './img/',
  dpi = 100,
  progress = TRUE
)
```

# single-cell RNA sequencing
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

Most of the materials from this practical come from the following sources:

- [The Orchestrating Single-Cell Analysis with Bioconductor book](https://osca.bioconductor.org/)
- [The Spatter vignettes](https://bioconductor.org/packages/release/bioc/html/splatter.html)
- [The Seurat vignettes](https://satijalab.org/seurat/vignettes.html)
- [The Kallisto-Bustools tutorials](https://www.kallistobus.tools/tutorials)
- [The Bioconductor vignettes of the R packages used](http://www.bioconductor.org/)
- [The monocle vignette](http://cole-trapnell-lab.github.io/monocle-release/monocle3/#tutorial-1-learning-trajectories-with-monocle-3)
- [The TradSeq vignette](https://statomics.github.io/tradeSeq/articles/tradeSeq.html)


Please run the following command before the practical:

In a bash shell:

```sh
singularity run /data/share/MADT/TP_lmodolo/lbmc-tp_scrna\:0.2.0.sif
# singularity run docker://lbmc/docker://lbmc/tp_scrna:0.2.0
```

All the materials are available here: [https://gitbio.ens-lyon.fr/LBMC/hub/formations/scrna](https://gitbio.ens-lyon.fr/LBMC/hub/formations/scrna)

# Summary

- [Introduction](1_introduction/introduction.html)
- [Nature of the data](2_nature_of_the_data/nature_of_the_data.html)
- [RNA Quantification](3_rna_quantification/rna_quantification.html)
- [SingleCellExperiment](4_singlecellexperiment/singecellexperiment.html)
- [Quality Control](5_quality_control/quality_control.html)
- [Normalization](6_normalization/normalization.html)
- [Data analysis](7_data_analysis/data_analysis.html)
- [Trajectory analysis](8_trajectory_analysis/trajectory_analysis.html)