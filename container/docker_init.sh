#!/bin/sh
docker pull lbmc/tp_scrna:0.2.0
docker build container -t 'lbmc/tp_scrna:0.2.0'
docker push lbmc/tp_scrna:0.2.0
singularity build lbmc-tp_scrna:0.2.0.sif docker://lbmc/tp_scrna:0.2.0
singularity run lbmc-tp_scrna:0.2.0.sif
